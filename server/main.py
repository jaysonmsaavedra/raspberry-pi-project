from flask import Flask
from flask import jsonify
from flask import request

app = Flask(__name__)

def toggle_item(itemID):
    # toggle the item on or off
    return jsonify(
        success=True,
        itemID=itemID
    )

@app.route("/")
def hello():
    return jsonify(
        name="jayson",
        email="jaysonmsaavedra"
    )

@app.route("/item/<itemID>", methods=['POST'])
def handle_request(itemID):
    if request.method == 'POST':
        return toggle_item(itemID)

if __name__ == "__main__":
    app.run()